package me.msz.learning.rest.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Root resource (exposed at "hello" path)
 */
@Path("/hello")
public class HelloResource {
	/**
	 * Method handling HTTP GET requests. The returned object will be sent
	 * to the client as "text/html" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response SayHello() {
		String retEntity = "Hello, REST World!";
		return Response.status(200).entity(retEntity).encoding("UTF-8").build();
	}

	/**
	 * Method handling HTTP GET requests. The returned object will be sent
	 * to the client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/to-somebody")
	public Response SayHelloQuery(@QueryParam("name") String name) {
		String retEntity = "Hello, " + name + ". Welcome to the REST world!" + " From query parameter.";
		return Response.status(200).entity(retEntity).encoding("UTF-8").build();
	}

	@GET
	@Path("/to-somebody/{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response SayHelloPath(@PathParam("name") String name) {
		String retEntity = "Hello, " + name + ". Welcome to the REST world!" + " From path parameter.";
		return Response.status(200).entity(retEntity).encoding("UTF-8").build();
	}
}
