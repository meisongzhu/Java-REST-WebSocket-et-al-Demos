package me.msz.learning.rest;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Created by Erich on 2016/1/6.
 */
public class MszApplication extends ResourceConfig {
	public MszApplication() {
		packages("me.msz.learning.rest.resources");
		register(JacksonFeature.class);
	}
}
