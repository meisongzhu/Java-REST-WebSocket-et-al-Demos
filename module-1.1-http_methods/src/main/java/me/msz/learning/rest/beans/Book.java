package me.msz.learning.rest.beans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by Erich on 2016/1/6.
 */
@ApiModel(value = "Book", description = "书籍类")
public class Book {
	public Book() {
	}

	public Book(String title, String authors, double price) {
		this.title = title;
		this.authors = authors;
		this.price = price;
	}

	@ApiModelProperty(value = "书籍名称")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@ApiModelProperty(value = "书籍作者")
	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	@ApiModelProperty(value = "书籍定价")
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Book Title: " + this.title + "\n" +
				"Book Author(s): " + this.authors + "\n" +
				"Book Price: " + this.price;
	}

	private String title;
	private String authors;
	private double price;
}
