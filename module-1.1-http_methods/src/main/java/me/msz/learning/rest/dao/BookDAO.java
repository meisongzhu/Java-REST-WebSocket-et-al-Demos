package me.msz.learning.rest.dao;

import me.msz.learning.rest.beans.Book;

import java.util.List;

/**
 * Created by Erich on 2016/1/6.
 */
public interface BookDAO {
	//	@SqlUpdate("insert into books (id, title, authors, price) values (:id, :title, :authors, :price)")
	void createBook(Book book);

	//	@SqlQuery("select * from book where title = :title")
	List<Book> getByTitile(String title);

	List<Book> getAllBooks();

	void updateBook(Book book);

	void deleteBook(Book book);

	void deleteAllBooks();
}
