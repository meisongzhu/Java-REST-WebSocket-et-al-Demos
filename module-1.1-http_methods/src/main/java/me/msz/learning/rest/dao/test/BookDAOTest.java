package me.msz.learning.rest.dao.test;

import me.msz.learning.rest.beans.Book;
import me.msz.learning.rest.dao.BookDAO;
import me.msz.learning.rest.dao.impl.BookDAOImplH2JDBC;

import java.util.List;

/**
 * Created by Erich on 2016/1/7.
 */
public class BookDAOTest {
	public static void main(String[] args) {
		BookDAO dao = BookDAOImplH2JDBC.getInstance();

		Book book1 = new Book("book1", "author1", 1.11);
		Book book2 = new Book("book2", "author2", 2.22);
		Book book3 = new Book("book3", "author3", 3.33);
		Book book4 = new Book("book4", "author4", 4.44);

		dao.createBook(book1);
		dao.createBook(book2);

		List<Book> bookList = dao.getAllBooks();
		for (Book book : bookList) {
			System.out.println(book.toString());
		}
		System.out.println("--------------Query 1---------------");

		book2.setPrice(2.44);
		dao.updateBook(book2);
		bookList = dao.getByTitile("book2");
		for (Book book : bookList) {
			System.out.println(book.toString());
		}
		System.out.println("--------------Query 2---------------");

		dao.createBook(book3);
		dao.createBook(book4);
		bookList = dao.getAllBooks();
		for (Book book : bookList) {
			System.out.println(book.toString());
		}
		System.out.println("--------------Query 3---------------");

		dao.deleteAllBooks();
		bookList = dao.getAllBooks();
		for (Book book : bookList) {
			System.out.println(book.toString());
		}
		System.out.println("--------------Query 4---------------");
	}
}
