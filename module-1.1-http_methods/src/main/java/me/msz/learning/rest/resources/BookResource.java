package me.msz.learning.rest.resources;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.util.StatusPrinter;
import io.swagger.annotations.*;
import me.msz.learning.rest.beans.Book;
import me.msz.learning.rest.dao.impl.BookDAOImplH2JDBC;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import ch.qos.logback.classic.LoggerContext;
//import ch.qos.logback.core.util.StatusPrinter;

/**
 * Root resource (exposed at "books" path)
 */
@Path("/books")
@Api(value = "/books", description = "图书管理示例程序服务接口")
public class BookResource {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "查询书籍",
			notes = "查询所有的书籍列表",
			response = Book.class,
			responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "")})
	public Response getAllBooks() {
		logger.info("GET: Query All Books");
		List<Book> listBooks = BookDAOImplH2JDBC.getInstance().getAllBooks();
		return Response.status(200).entity(listBooks).encoding("UTF-8").build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "增加一本书",
			notes = "增加一本书，并返回所有的书籍列表",
			response = Book.class,
			responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "")})
	public Response addABook(@ApiParam(value = "增加的书的对象", required = true) Book book) {
		logger.info("POST: Add New Book: {}\t{}\t{}", book.getTitle(), book.getAuthors(), book.getPrice());
		BookDAOImplH2JDBC.getInstance().createBook(book);
		List<Book> listBooks = BookDAOImplH2JDBC.getInstance().getAllBooks();
		return Response.status(200).entity(listBooks).encoding("UTF-8").build();
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "删除一本书",
			notes = "删除一本书，并返回所有的书籍列表",
			response = Book.class,
			responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "")})
	public Response deleteAllBooks() {
		logger.info("DELETE: Delete All Books");
		BookDAOImplH2JDBC.getInstance().deleteAllBooks();
		List<Book> listBooks = BookDAOImplH2JDBC.getInstance().getAllBooks();
		return Response.status(200).entity(listBooks).encoding("UTF-8").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/book/{title}")
	@ApiOperation(value = "查找书籍",
			notes = "根据书名查找书籍，并返回具有该名称的书籍列表",
			response = Book.class,
			responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "")})
	public Response getABook(@ApiParam(value = "书籍标题", required = true) @PathParam("title") String title) {
		logger.info("GET: Query Book Which Title is {}", title);
		List<Book> listBooks = BookDAOImplH2JDBC.getInstance().getByTitile(title);
		return Response.status(200).entity(listBooks).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/book")
	@ApiOperation(value = "更新书籍信息",
			notes = "更新书籍信息，并返回书籍列表",
			response = Book.class,
			responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "")})
	public Response updateABook(@ApiParam(value = "书籍对象", required = true) Book book) {
		logger.info("PUT: Update Book {}\t{}\t{}", book.getTitle(), book.getAuthors(), book.getPrice());
		BookDAOImplH2JDBC.getInstance().updateBook(book);
		List<Book> listBooks = BookDAOImplH2JDBC.getInstance().getAllBooks();
		return Response.status(200).entity(listBooks).encoding("UTF-8").build();
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/book")
	@ApiOperation(value = "删除书籍",
			notes = "删除书籍，并返回书籍列表",
			response = Book.class,
			responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "")})
	public Response deleteABook(@ApiParam(value = "书籍对象", required = true) Book book) {
		logger.info("DELETE: Delete Book {}\t{}\t{}", book.getTitle(), book.getAuthors(), book.getPrice());
		BookDAOImplH2JDBC.getInstance().deleteBook(book);
		List<Book> listBooks = BookDAOImplH2JDBC.getInstance().getAllBooks();
		return Response.status(200).entity(listBooks).encoding("UTF-8").build();
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/book/{title}")
	@ApiOperation(value = "删除书籍",
			notes = "根据书名删除书籍，并返回书籍列表",
			response = Book.class,
			responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "")})
	public Response deleteABook(@ApiParam(value = "书籍标题", required = true) @PathParam("title") String title) {
		Book book = new Book();
		book.setTitle(title);
		logger.info("DELETE: Delete Book Which Title is {}", book.getTitle());
		BookDAOImplH2JDBC.getInstance().deleteBook(book);
		List<Book> listBooks = BookDAOImplH2JDBC.getInstance().getAllBooks();
		return Response.status(200).entity(listBooks).encoding("UTF-8").build();
	}

	private Logger logger;

	{
		logger = LoggerFactory.getLogger(me.msz.learning.rest.resources.BookResource.class);
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		StatusPrinter.print(lc);
	}
}
